# pycontinuum: never press a re-run button again.

`pycontinuum` is a development server based around the update-and-rerun model of coding; when you edit the codebase, `pycontinuum` automatically computes the changes to the application and updates the application to match changes.

Basically, in other words, it's `python -m project` or `python project.py` on steroids - a lot of steroids.

To use:
``
