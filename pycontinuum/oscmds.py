import logging
import os
import sys
import signal
import subprocess
import time

def run_oscmd_safe(cmd, env=os.environ):
    try:
        p = subprocess.Popen(cmd.split(), preexec_fn=os.setsid, env=env)
        return p.pid
    except subprocess.CalledProcessError:
        logging.fatal("E: There was an error running command `{}`. The server must exit."
                      .format(cmd))
        sys.exit(1)


def check_executable(path, version=3):
    """
    Checks if a certain file is executable by Python.
    """
    ptype = "package"if os.path.isdir(path) else "module"

    cmd = "python3 -m " if ptype == "package" else "python3 "
    print(cmd, ptype)
    if version == 2:
        cmd.replace("3", "2")
    cmd += path

    try:
        p = subprocess.Popen(cmd.split(), stdout=None, shell=True, preexec_fn=os.setsid)
        os.killpg(os.getpgid(p.pid), signal.SIGTERM)
    except subprocess.CalledProcessError:
        logging.fatal("E: The Python project is not runnable. Exiting.")
        sys.exit(1)


def check_project_type(path):
    if os.path.isdir(path):
        check_executable(path)
        return "package"
    else:
        check_executable(path)
        return "module"
