import logging
import os
import sys
import time
from pynput import keyboard
from pycontinuum.oscmds import run_oscmd_safe, check_executable
from pycontinuum.handler import Handler
from watchdog.observers import Observer
from watchdog.events import FileSystemEvent

# Watch for the program run sequence (Ctrl-R)
RUN_COMBO = {keyboard.Key.ctrl,
             keyboard.KeyCode.from_char("r")}
# Watch for the program terminate sequence (Ctrl-E)
EXIT_COMBO = {keyboard.Key.ctrl,
              keyboard.KeyCode.from_char("e")}
current = set()

def on_press(key):
    if key in set.union(RUN_COMBO, EXIT_COMBO):
        current.add(key)
        print(current)
        if all(k in current for k in RUN_COMBO):
            # Run project manually
            event_handler.run_project_manual()
        if all(k in current for k in EXIT_COMBO):
            # Terminate current program
            event_handler.commit_oof()


def on_release(key):
    if key in set.union(RUN_COMBO, EXIT_COMBO):
        try:
            current.remove(key)
        except KeyError:
            pass

#--------------------------
# Mainloop code

def mainloop(project, ptype, version):
    global event_handler # declare global so kill keypress works

    print("Starting mainloop...")
    project = os.path.abspath(project)


    event_handler = Handler(project, ptype, version)
    observer = Observer()

    if os.path.isdir(project):
        # Directory, schedule "normally"
        observer.schedule(event_handler, project, recursive=True)
    else:
        # We have to watch the directory the file is in
        observer.schedule(event_handler, "/".join(project.split("/")[:-1]), recursive=True)

    observer.start()

    try:
        # Trigger a build manually using dummy event
        # TODO: Perhaps a less hacky way of doing this?
        event_handler.run_project_manual()

        # Start keyboard listener
        with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
            listener.join()

        while True:
            # Listen for program terminate seq (Ctrl-E)
            time.sleep(0.2)


    except KeyboardInterrupt:
        observer.stop()
        event_handler.commit_oof()

    observer.join()
    sys.exit(0)
