import logging
import os
import signal
import time
import pynput
from pycontinuum.oscmds import run_oscmd_safe
from watchdog.events import FileSystemEventHandler, FileSystemEvent

class Handler(FileSystemEventHandler):
    def __init__(self, project, ptype, pyversion):
        self.project = os.path.abspath(project)
        self.prefix = ""
        self.ptype = ptype
        self.pyversion = pyversion
        self.pid = -1
        self.last_called = 0

    def run_project_manual(self):
        dummy_event = FileSystemEvent(self.project)
        self.on_modified(dummy_event)

    def on_modified(self, event):
        # Any time the project, changes, rerun


        if time.time() - self.last_called > 1:
            if self.pid != -1:
                try:
                    print("killing", self.pid)
                    os.killpg(os.getpgid(self.pid), signal.SIGTERM)
                except ProcessLookupError:
                    pass

        if self.pyversion in {2, 3}:
            if time.time() - self.last_called < 1:
                # Ignore multiple generated events
                return

            logging.info("I: Running the new project. Stop execution at any time with Ctrl-E.")
            self.pid = run_oscmd_safe("python{} ".format(self.pyversion)
                                      + self.project)

        else:
            logging.ERROR("The Python version is set incorrectly - not running.")

        self.last_called = time.time()

    def commit_oof(self):
        try:
            os.killpg(os.getpgid(self.pid), signal.SIGTERM)
            run_oscmd_safe("clear")
            time.sleep(0.05)
            logging.getLogger().setLevel(logging.INFO)
            logging.info("I: Terminated program. Program will be rerun on next edit.")
        except ProcessLookupError:
            pass
