#!/usr/bin/env python3
import os
import sys
import time
import argparse
import importlib.util
from pycontinuum.oscmds import run_oscmd_safe, check_executable, check_project_type
from pycontinuum.runloop import mainloop

VERSION = 0.1

def main():
    # Add arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("project", help="Path to a Python file or directory.")
    parser.add_argument("--python2", action="store_true", help="Run with Python2 instead of 3.")
    args = parser.parse_args()
    project = args.project
    pyversion = 2 if args.python2 else 3

    # Start server
    print("Starting PyContinuum version {}".format(VERSION))
    print("Note that PyContinuum does not safety-check projects. Make sure the Python project you're running is safe to run.")
    print()
    time.sleep(0.25)
    print("Identifying Python project...")
    print()

    # Identify project type
    print("Project Location: " + os.path.abspath(project))
    project_type = check_project_type(project)
    print("Done!")

    mainloop(args.project, project_type, pyversion)

if __name__ == "__main__":
    main()
